;; .emacs

;;; Code:



;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(global-eldoc-mode -1)

(add-hook 'minibuffer-setup-hook
    (lambda () (setq truncate-lines nil)))

(setq resize-mini-windows t) ; grow and shrink as necessary
(setq max-mini-window-height 10) ; grow up to max of 10 lines

(setq minibuffer-scroll-window t)

(add-to-list 'exec-path
  "/usr/local/bin"
)
(add-to-list 'exec-path
  "/home/suppi/.local/bin"
  )

;; load packages
(load "~/.emacs.d/my-loadpackages.el")

(defun pbcopy ()
    (interactive)
      (call-process-region (point) (mark) "pbcopy")
        (setq deactivate-mark t))

(defun pbpaste ()
    (interactive)
      (call-process-region (point) (if mark-active (mark) (point)) "pbpaste" t t))

(defun pbcut ()
    (interactive)
      (pbcopy)
        (delete-region (region-beginning) (region-end)))

(global-set-key (kbd "C-c c") 'pbcopy)
(global-set-key (kbd "C-c v") 'pbpaste)
(global-set-key (kbd "C-c x") 'pbcut)

;;; uncomment this line to disable loading of "default.el" at startup
;; (setq inhibit-default-init t)

;; enable visual feedback on selections
;(setq transient-mark-mode t)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)

;; line numbers
(global-linum-mode 1)

;; font
(set-frame-font "Anonymous Pro-16")

;; scrolling
(setq scroll-step 1
   scroll-conservatively  10000)

;; ido mode for narrowing lists and such
(ido-mode 1)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)

;; auto indent
(define-key global-map (kbd "RET") 'newline-and-indent)

(define-key global-map (kbd "<f10>") 'revert-buffer)

;; parens
(show-paren-mode 1)

;;; Ben recommendations

;; Warn before you exit emacs!
(setq confirm-kill-emacs 'yes-or-no-p)

;; Don't insert instructions in the *scratch* buffer
(setq initial-scratch-message "")
(setq initial-major-mode 'emacs-lisp-mode)

;; make all "yes or no" prompts show "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

;; I use version control, don't annoy me with backup files everywhere
(setq make-backup-files nil)
(setq auto-save-default nil)

(defun show-notification (notification)
  "Show notification. Use notify-send."
  (start-process "notify-send" nil "notify-send" "-i" "/usr/local/share/emacs/24.5/etc/images/icons/hicolor/32x32/apps/emacs.png" notification)
)

(defun notify-compilation-result (buffer msg)
  "Notify that the compilation is finished"
  (if (equal (buffer-name buffer) "*compilation*")
    (if (string-match "^finished" msg)
      (show-notification "Compilation Successful")
      (show-notification "Compilation Failed")
    )
  )
)

(add-to-list 'compilation-finish-functions 'notify-compilation-result)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(ansi-term-color-vector
   [unspecified "#151718" "#CE4045" "#9FCA56" "#DCCD69" "#55B5DB" "#A074C4" "#55B5DB" "#D4D7D6"])
 '(cursor-type (quote bar))
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "a2e7b508533d46b701ad3b055e7c708323fb110b6676a8be458a758dd8f24e27" "badc4f9ae3ee82a5ca711f3fd48c3f49ebe20e6303bba1912d4e2d19dd60ec98" "94ba29363bfb7e06105f68d72b268f85981f7fba2ddef89331660033101eb5e5" "0ad5a61e6ee6d2e7f884c0da7a6f437a4c84547514b509bdffd06757a8fc751f" "870a63a25a2756074e53e5ee28f3f890332ddc21f9e87d583c5387285e882099" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default)))
 '(fci-rule-color "#202325")
 '(flycheck-pos-tip-timeout 20)
 '(haskell-interactive-types-for-show-ambiguous nil)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type (quote stack-ghci))
 '(haskell-tags-on-save t)
 '(package-selected-packages
   (quote
    (elm-mode psc-ide flycheck-purescript purescript-mode intero flycheck-pos-tip flycheck company popup seti-theme flatland-theme color-theme-solarized eyebrowse evil)))
 '(psc-ide-executable "~/.local/bin/psc-ide" t)
 '(vc-annotate-background "#1f2124")
 '(vc-annotate-color-map
   (quote
    ((20 . "#ff0000")
     (40 . "#ff4a52")
     (60 . "#f6aa11")
     (80 . "#f1e94b")
     (100 . "#f5f080")
     (120 . "#f6f080")
     (140 . "#41a83e")
     (160 . "#40b83e")
     (180 . "#b6d877")
     (200 . "#b7d877")
     (220 . "#b8d977")
     (240 . "#b9d977")
     (260 . "#93e0e3")
     (280 . "#72aaca")
     (300 . "#8996a8")
     (320 . "#afc4db")
     (340 . "#cfe2f2")
     (360 . "#dc8cc3"))))
 '(vc-annotate-very-old-color "#dc8cc3"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
