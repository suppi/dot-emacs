; my-loadpackages.el
; loading package

;;; Code:

(load "~/.emacs.d/my-packages.el")

(require 'color-theme)
(set-frame-parameter nil 'background-mode 'dark)
(set-terminal-parameter nil 'background-mode 'dark)
(load-theme 'seti t)

(add-hook 'after-init-hook 'global-company-mode)


(global-set-key (kbd "C-c C-w") 'company-complete)
(setq company-minimum-prefix-length 2)

(require 'flycheck)

(add-hook 'after-init-hook #'global-flycheck-mode)

(require 'flycheck-pos-tip)
(with-eval-after-load 'flycheck
  (flycheck-pos-tip-mode))

;;;;;;;;;;;;;;;;
;;            ;;
;; PURESCRIPT ;;
;;            ;;
;;;;;;;;;;;;;;;;

(require 'psc-ide)

(add-hook 'purescript-mode-hook
  'turn-on-purescript-indentation)

(add-hook 'purescript-mode-hook
  'psc-ide-mode)

(add-hook 'purescript-mode-hook
  'company-mode)


;;;;;;;;;;;;;
;;         ;;
;; HASKELL ;;
;;         ;;
;;;;;;;;;;;;;

(require 'intero)
(add-hook 'haskell-mode-hook 'intero-mode)

(global-set-key (kbd "C-c w") 'company-complete)

(global-set-key [f9] 'flycheck-list-errors)

(require 'haskell-mode)

(define-key haskell-mode-map (kbd "C-`") 'haskell-interactive-bring)
(define-key haskell-mode-map (kbd "C-l C-l") 'haskell-process-load-or-reload)
(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-process-stack-build)
(define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
(define-key haskell-mode-map (kbd "C-c c") 'haskell-process-stack)
;(define-key haskell-mode-map (kbd "C-c w") 'company-complete)
(define-key haskell-mode-map (kbd "C-c i i") 'haskell-navigate-imports)
(define-key haskell-mode-map (kbd "C-c i f") 'haskell-mode-format-imports)
(define-key haskell-mode-map (kbd "C-c i s") 'haskell-sort-imports)
(define-key haskell-mode-map (kbd "C-c i a") 'haskell-align-imports)

(require 'evil)
(evil-mode 1)

(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-visual-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-visual-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil)
  (define-key evil-visual-state-map (kbd "TAB") nil))

(with-eval-after-load 'evil-maps
  (define-key evil-normal-state-map (kbd "C-.") nil)
  (define-key evil-normal-state-map (kbd "M-.") nil))

;;;


(require 'elm-mode)
(require 'elixir-mode)
(require 'rust-mode)
(add-hook 'rust-mode-hook 'cargo-minor-mode)
(add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
